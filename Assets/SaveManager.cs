using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public static class SaveManager
{
    public static void SaveTopScores(TopScores_Class tops)
    {
        string dataPath = Application.persistentDataPath + "/TopScores.save";

        FileStream fS = new FileStream(dataPath, FileMode.Create);
        BinaryFormatter bF = new BinaryFormatter();

        bF.Serialize(fS, tops);
        fS.Close();
    }

    public static TopScores_Class LoadTopScores()
    {
        string dataPath = Application.persistentDataPath + "/TopScores.save";

        if (File.Exists(dataPath))
        {
            FileStream fS = new FileStream(dataPath, FileMode.Open);
            BinaryFormatter bF = new BinaryFormatter();
            TopScores_Class tops = (TopScores_Class)bF.Deserialize(fS);
            fS.Close();
            return tops;

        } else return null;    
    }
}
