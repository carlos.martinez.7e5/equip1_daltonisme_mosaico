using UnityEngine;

[System.Serializable]

public class TopScores_Class
{
    public int _azules;
    public int _verdes;
    public int _rojos;
    

    public TopScores_Class()
    {
        _azules = 0;
        _verdes = 0;
        _rojos = 0;
    }
}
