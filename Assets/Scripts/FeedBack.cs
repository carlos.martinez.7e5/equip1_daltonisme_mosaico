using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FeedBack : MonoBehaviour
{
    AudioSource _audioSource;
    
    [SerializeField]
    AudioClip[] audio;
    
    // Start is called before the first frame update
    void Start()
    {
        _audioSource = gameObject.GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PlayWrongAnswerSound()
    {
        _audioSource.clip = audio[0];
        _audioSource.Play();
    }

    public void PlayCorrectAnswerSound()
    {
        _audioSource.clip = audio[1];
        _audioSource.Play();
    }
}
