using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionsCanvasManager : MonoBehaviour
{
    [SerializeField]
    Slider _vFXSlider;
    [SerializeField]
    Slider _musicSlider;
    [SerializeField]
    Slider _shakeSlider;

    GameObject _vFXSoundController;
    GameObject _MusicSoundController;
    GameObject _CameraShakeController;

    // Start is called before the first frame update
    void Awake()
    {
         _vFXSoundController = GameObject.Find("VFXAudioSource");
        _MusicSoundController = GameObject.Find("MusicAudioSource");
        _CameraShakeController = GameObject.Find("CameraShaker");

        _vFXSlider.value = _vFXSoundController.GetComponent<AudioSource>().volume;
        _musicSlider.value = _MusicSoundController.GetComponent<AudioSource>().volume;
        _shakeSlider.value = _CameraShakeController.GetComponent<CameraShake>().shakeAmount;
    }

  

    // Update is called once per frame
    void Update()
    {
        //VFX
        _vFXSoundController.GetComponent<AudioSource>().volume = _vFXSlider.value; //<--
        _vFXSlider.transform.Find("PorcentageText").GetComponent<Text>().text = (int)(_vFXSlider.value * 100) +"%";

        //Music
        _MusicSoundController.GetComponent<AudioSource>().volume = _musicSlider.value;
        _musicSlider.transform.Find("PorcentageText").GetComponent<Text>().text = (int)(_musicSlider.value * 100) + "%";

        //Shake
        _CameraShakeController.GetComponent<CameraShake>().shakeAmount = _shakeSlider.value;
        _shakeSlider.transform.Find("PorcentageText").GetComponent<Text>().text = (int)(_shakeSlider.value * 100) + "%";
    }

    public void DesactivateOptionCanvas(){
        if (GameObject.Find("Player") != null)
            GameObject.Find("Player").GetComponent<Raycast>().enabled = true;

        this.gameObject.SetActive(false);
    }

    public void ActiveOptionsCanvas()
    {
        if (GameObject.Find("Player") != null)
            GameObject.Find("Player").GetComponent<Raycast>().enabled = false;

        this.gameObject.SetActive(true);
    }
}
