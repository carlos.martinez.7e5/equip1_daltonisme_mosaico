using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasManager : MonoBehaviour
{

    //Script para modificar el porcentaje en pantalla mientras se hace el mosaico

    // Start is called before the first frame update
    [SerializeField]
    Text _porcentageText;

    [SerializeField]
    GameObject _optionsCanvas;


    void Start()
    {
        SetPorcentageText(0);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetPorcentageText(int porcentage)
    {
        _porcentageText.text = porcentage + "%";
    }
}
