using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderManager : MonoBehaviour
{
    [SerializeField] private Slider _slider; //Slider para cambiar el valor y mostrar el resultado
    [SerializeField] private GameObject _sliderFill; //La "imagen" de fondo del slider, para cambiarle el color seg�n el test hecho
    [SerializeField] private Text _porcentajeTxt; //El texto que muestra el porcentaje obtenido
    [SerializeField] private Text _colorTxt; //El texto que muestra de qu� color se muestran los resultados
    
    private GameObject _dataManager; //Objeto que recoge el GameObject DataManager, que es el que tiene los resultados

    void Start()
    {
        _dataManager = GameObject.Find("DataManager");

        //Dependiendo del estado (Color) del data manger que ha cogido
        switch (_dataManager.GetComponent<DataManagement>().CurrentColor)
        {
            /*1. Se cambia el valor del slider al valor del porcentaje de aciertos (del 0 al 100)
              2. Se cambia el color del slider, para que concuerde con el resultado mostrado
              3. Se cambia el texto que muestra el resultado al porcentaje de aciertos
              4. Se cambia el texto que muestra de que color se est�n dando los resultados (redundante con el color del slider, pero se pone igual) */
            case DataManagement.Colores.azul:
                _slider.value = _dataManager.GetComponent<DataManagement>().PorcentajeAzules; /*1*/   
                _sliderFill.GetComponent<Image>().color = UnityEngine.Color.blue; /*2*/
                _porcentajeTxt.text = _dataManager.GetComponent<DataManagement>().PorcentajeAzules + " %";  /*3*/
                _colorTxt.text = "Azul"; /*4*/
                break;
            case DataManagement.Colores.rojo:
                _slider.value = _dataManager.GetComponent<DataManagement>().PorcentajeRojos;
                _sliderFill.GetComponent<Image>().color = UnityEngine.Color.red;
                _porcentajeTxt.text = _dataManager.GetComponent<DataManagement>().PorcentajeRojos + " %";
                _colorTxt.text = "Rojo";
                break;
            case DataManagement.Colores.verde:
                _slider.value = _dataManager.GetComponent<DataManagement>().PorcentajeVerdes;
                _sliderFill.GetComponent<Image>().color = UnityEngine.Color.green;
                _porcentajeTxt.text = _dataManager.GetComponent<DataManagement>().PorcentajeVerdes + " %";
                _colorTxt.text = "Verde";
                break;
        }
    }
}
