using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DataManagement : MonoBehaviour
{
    [SerializeField] private int _azules = 0; //Porcentaje de azul
    [SerializeField] private int _rojos = 0; //Porcentaje de rojo
    [SerializeField] private int _verdes = 0; //Porcentaje de verde
   
    [SerializeField] private Colores _currentColor; //El estado (color) en el que se encuentra

    /*Para hacer un singleton haces una instancia est�tica. Ser� accesible desde todos lados, y ser� la "�nica" que se compartir�
    entre escenas */
    public static DataManagement Intance;

    /*Getters y setters para las propiedades de los colores. Para que sean privadas de esta clase pero se puedan consultar y modificar desde otras si es necesario.
     Por c�mo est� hecho deber�a ser imposible que se estableciesen valores mayores de 100 o menores de 0, pero por precauci�n se limitan */

    GameObject _canvasManager;

    public int PorcentajeAzules
    {
        get { return _azules;}
        set {
            if (_azules >= 100) _azules = 100;
            else if (_azules < 0) _azules = 0;
            else _azules = value; }
    }

    public int PorcentajeRojos
    {
        get { return _rojos; }
        set
        {
            if (_rojos >= 100) _rojos = 100;
            else if (_rojos < 0) _rojos = 0;
            else _rojos = value;
        }
    }

    public int PorcentajeVerdes
    {
        get { return _verdes; }
        set
        {
            if (_verdes >= 100) _verdes = 100;
            else if (_verdes < 0) _verdes = 0;
            else _verdes = value;
        }
    }

    /*Enum con los diferentes estados (colores) que puede "estar"*/
    public enum Colores
    {
        gris,
        rojo,
        verde,
        azul
    }

    /*Getter y setter de la propiedad del enum de Colores*/
    public Colores CurrentColor
    {
        get { return _currentColor; }
        set { _currentColor = value; }
    }

    private void Awake()
    {
        /*Para vigilar que no se duplique, en el awake mira si ya hay alguna. 
         Si s� que hay se destruye autom�ticamente para que solo haya una*/
        if (DataManagement.Intance == null)
        {
            //Si es null quiere decir que es la primera, por lo que se genera y se hace DontDestroyOnLoad
            DataManagement.Intance = this;
            DontDestroyOnLoad(this.gameObject);
        } else
        {
            //En caso contrario, quiere decir que ya hay otra, por lo que autom�ticamente se destruye
            Destroy(this.gameObject);
        }
    }

    /*Funci�n acertar. Suma +5 % al porcentaje del color en el que se encuentre*/
    public void Acertar()
    {
        _canvasManager = GameObject.Find("CanvasManager");
        
        switch (_currentColor)
        {
            case Colores.gris:
                CurrentColor = Colores.rojo;
                SceneManager.LoadScene("MosaicoScene");
                break;

            case Colores.rojo:
                PorcentajeRojos += 5;
                _canvasManager.GetComponent<CanvasManager>().SetPorcentageText(PorcentajeRojos); //Imprime el porcentaje en pantalla
                if (PorcentajeRojos >= 100) SceneManager.LoadScene("ResultScene");
                break;

            case Colores.verde:
                PorcentajeVerdes += 5;
                _canvasManager.GetComponent<CanvasManager>().SetPorcentageText(PorcentajeVerdes);
                if (PorcentajeVerdes >= 100) SceneManager.LoadScene("ResultScene");
                break;
                
            case Colores.azul:
                PorcentajeAzules += 5;
                _canvasManager.GetComponent<CanvasManager>().SetPorcentageText(PorcentajeAzules);
                if (PorcentajeAzules >= 100) SceneManager.LoadScene("RecordsScene");
                break;
        }
    }
}
