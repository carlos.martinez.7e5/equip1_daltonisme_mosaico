using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelColors : MonoBehaviour
{
    [SerializeField]
    public Color32[] arrayColorDiferent = new Color32[61];

    [SerializeField]
    public Color32[] arrayColorFons = new Color32[4];

    GameObject _dataManager;
    DataManagement.Colores color;
    public int DificultyLevel;

    // Start is called before the first frame update
    void Awake()
    {
        _dataManager = GameObject.Find("DataManager");

        color = _dataManager.GetComponent<DataManagement>().CurrentColor;

        switch (color)
        {
            case DataManagement.Colores.gris:
                gameObject.GetComponent<CambioDeLuminosidad>().SetDefaultColor(arrayColorFons[0]);
                gameObject.GetComponent<CambioDeLuminosidad>().SetAlternativeColor(arrayColorDiferent[0]);
                break;

            case DataManagement.Colores.rojo:
                gameObject.GetComponent<CambioDeLuminosidad>().SetDefaultColor(arrayColorFons[1]);
                gameObject.GetComponent<CambioDeLuminosidad>().SetAlternativeColor(arrayColorDiferent[1]);
                break;

            case DataManagement.Colores.verde:
                gameObject.GetComponent<CambioDeLuminosidad>().SetDefaultColor(arrayColorFons[2]);
                gameObject.GetComponent<CambioDeLuminosidad>().SetAlternativeColor(arrayColorDiferent[21]);
                break;
            case DataManagement.Colores.azul:
                gameObject.GetComponent<CambioDeLuminosidad>().SetDefaultColor(arrayColorFons[3]);
                gameObject.GetComponent<CambioDeLuminosidad>().SetAlternativeColor(arrayColorDiferent[41]);
                break;
        }

    }

    public void ChangeDificulty()
    {
        DificultyLevel++;
        switch (color)
        {
            case DataManagement.Colores.rojo:
                gameObject.GetComponent<CambioDeLuminosidad>().SetAlternativeColor(arrayColorDiferent[1 + DificultyLevel]);
                break;
            case DataManagement.Colores.verde:
                gameObject.GetComponent<CambioDeLuminosidad>().SetAlternativeColor(arrayColorDiferent[21 + DificultyLevel]);
                break;
            case DataManagement.Colores.azul:
                gameObject.GetComponent<CambioDeLuminosidad>().SetAlternativeColor(arrayColorDiferent[41 + DificultyLevel]);
                break;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
