using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : MonoBehaviour
{
    // Camera Information
    private Vector3 orignalCameraPos;

    // Shake Parameters
    public float shakeDuration = 2f;
    public float shakeAmount = 0.7f;

    private bool canShake = false;
    private float _shakeTimer;



    // Start is called before the first frame update
    void Start()
    {
        orignalCameraPos = Camera.main.transform.localPosition;
    }

    // Update is called once per frame
    void Update()
    {
        if (canShake)
        {
            StartCameraShakeEffect();
        }
    }

    public void ShakeCamera()
    {
        canShake = true;
        _shakeTimer = 0;
    }

    void StartCameraShakeEffect()
    {
        if (_shakeTimer < shakeDuration)
        {
            Camera.main.transform.localPosition = orignalCameraPos + Random.insideUnitSphere * shakeAmount;
            _shakeTimer += Time.deltaTime;
        }
        else
        {
            _shakeTimer = 0f;
            Camera.main.transform.localPosition = orignalCameraPos;
            canShake = false;
        }
    }
}
