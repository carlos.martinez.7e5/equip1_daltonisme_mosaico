using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MosaicoForm : MonoBehaviour
{
    public float SquareScale = 2;
    private float _spriteLenght;
    private float _mosaicoLenght;
    public int MosaicoSize = 5;
    private GameObject[,] Mosaico;
    public GameObject Square; //selected in the editor
    private int AnswerLength = 3;

    // Start is called before the first frame update
    void Start()
    {
        _spriteLenght = Square.GetComponent<BoxCollider2D>().size.x * SquareScale;
        _mosaicoLenght = _spriteLenght * MosaicoSize;
        CreateMosaico();
        GenerateAnswer();
    }

    void CenterMosaicoInScreen()
    {
        gameObject.transform.position = new Vector3(-_mosaicoLenght/2, -_mosaicoLenght / 2);
    }

    public Color32 _color = new Color32(0, 0, 0, 255);

    // Update is called once per frame
    void Update()
    {
        
    }

    void CreateMosaico()
    {
        Mosaico = new GameObject[MosaicoSize, MosaicoSize];
        int cont = 0;

        for (int j = 0; j < MosaicoSize; j++)
        {
            for (int i = 0; i < MosaicoSize; i++)
            {
                GameObject _square = Instantiate(Square, new Vector3(this.gameObject.transform.position.x + (float)_spriteLenght * i, this.gameObject.transform.position.y + (float)_spriteLenght * j, 0), Quaternion.identity);
                _square.transform.localScale = Vector3.one;
                _square.name = "Square_" + cont;               
                _square.transform.localScale = new Vector3(SquareScale, SquareScale);
                
                Mosaico[i, j] = _square;
                cont++;
            }
        }
    }

    public void GenerateAnswer()
    {
        ResetMosaico();
        int[] position = GenerateRandomPosition();
        //Debug.Log(position[0] + " " + position[1]);
        int _answerLenght = AnswerLength / 2;

        for (int j = -_answerLenght; j <= _answerLenght; j++)
        {
            for (int i = -_answerLenght; i <= _answerLenght; i++)
            {
                Mosaico[position[0] - i, position[1] - j].gameObject.GetComponent<CambioDeLuminosidad>().ChangeToAlterantiveColor();
                Mosaico[position[0] - i, position[1] - j].gameObject.tag = "CorrectSquare";
            }
        }
    }

    public void ResetMosaico()
    {
        foreach(GameObject square in Mosaico)
        {
            square.GetComponent<CambioDeLuminosidad>().ChangeToDefaultColor();
            square.tag = "WrongSquare";
        }
    }

    private int[] GenerateRandomPosition() 
    {
        System.Random rnd = new System.Random();
        return new int[] { rnd.Next(1, MosaicoSize - 1) , rnd.Next(1, MosaicoSize - 1) };
    }

    public void Dificulty()
    {
        foreach(GameObject square in Mosaico)
            square.GetComponent<LevelColors>().ChangeDificulty();
    }
}
