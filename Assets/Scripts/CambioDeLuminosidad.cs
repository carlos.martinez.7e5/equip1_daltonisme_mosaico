using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CambioDeLuminosidad : MonoBehaviour
{

    public Color DefaultColor = new Color32(0,0,0,0);
    public Color AlternativeColor = new Color32(0,0,0,0);
    private bool _claroOscuro = true;
    private float _vComponent;
    private float _velChange;

    Color currColor;
    private float h, s, v;
    private float newH, newS, newV;

    // Start is called before the first frame update
    void Awake()
    {
        _velChange = (int)Random.Range(1, 10);

        _velChange /= 10;

        _vComponent = (int)Random.Range(50, 100);
        _vComponent /= 100;
        gameObject.GetComponent<SpriteRenderer>().color = DefaultColor;

        ChangeToDefaultColor();
    }

    // Update is called once per frame
    void Update()
    {
        ChangeLuminosity();
        //Debug.Log(_claroOscuro);
    }

    public void ChangeLuminosity()
    {
        if (_claroOscuro == true)
        {
            _vComponent -= _velChange * Time.deltaTime;
            //Debug.Log(_vComponent);
            gameObject.GetComponent<SpriteRenderer>().color = Color.HSVToRGB(h, s, _vComponent);
            currColor = gameObject.GetComponent<SpriteRenderer>().color;
            Color.RGBToHSV(currColor, out newH, out newS, out newV);
            if (newV <= 0.5) _claroOscuro = false;
        }
        else if (_claroOscuro == false)
        {
            _vComponent += _velChange * Time.deltaTime;
            //Debug.Log(_vComponent);
            gameObject.GetComponent<SpriteRenderer>().color = Color.HSVToRGB(h, s, _vComponent);
            currColor = gameObject.GetComponent<SpriteRenderer>().color;
            Color.RGBToHSV(currColor, out newH, out newS, out newV);
            if (newV >= 1) _claroOscuro = true;
        }
    }


    public void SetDefaultColor(Color newDefaultColor)
    {
        DefaultColor = newDefaultColor;
        ChangeToDefaultColor();
    }

    public void SetAlternativeColor(Color newAlternaitiveColor)
    {
        AlternativeColor = newAlternaitiveColor;
    }


    public void ChangeToAlterantiveColor()
    {
        Color.RGBToHSV(AlternativeColor, out h, out s, out v);
    }

    public void ChangeToDefaultColor()
    {
        Color.RGBToHSV(DefaultColor, out h, out s, out v);
    }
}
