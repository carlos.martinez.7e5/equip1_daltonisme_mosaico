using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ErrorCounter : MonoBehaviour
{

    public int maxErrors;
    GameObject _sceneManager;
    GameObject _dataManagement;

    public int errorCounter()
    {
        return _errorCounter;
    }

    public void AddError()
    {
        _errorCounter++;
    }

    public void ResetErrorCounter()
    {
        _errorCounter = 0;
    }



    private int _errorCounter;

    GameObject _mosaico;

    // Start is called before the first frame update
    void Start()
    {
        _mosaico = GameObject.Find("Mosaico");
        _sceneManager = GameObject.Find("SceneManager");
        _dataManagement = GameObject.Find("DataManager");
        Debug.Log("Hola");
        _errorCounter = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (_errorCounter == maxErrors)
        {
            ResetErrorCounter();
            
            if (_dataManagement.GetComponent<DataManagement>().CurrentColor == DataManagement.Colores.gris)
            {
                _dataManagement.GetComponent<DataManagement>().CurrentColor = DataManagement.Colores.rojo;
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
                Debug.Log(SceneManager.GetActiveScene().name);
            }
            else
                SceneManager.LoadScene("ResultScene");
        }
    }
}
