using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Raycast : MonoBehaviour
{
    RaycastHit2D hit;

    GameObject _mosaico;
    GameObject _dataManager;

    GameObject _VfxsoundController;
    GameObject _cameraShaker;

    // Start is called before the first frame update
    void Start()
    {
        _mosaico = GameObject.Find("Mosaico");
        _cameraShaker = GameObject.Find("CameraShaker");
        _VfxsoundController = GameObject.Find("VFXAudioSource");
        _dataManager = GameObject.Find("DataManager");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero, 10f);

            if (hit.collider.tag == "WrongSquare")
            {
                Debug.Log("Mal");
                _VfxsoundController.GetComponent<FeedBack>().PlayWrongAnswerSound();
                _cameraShaker.GetComponent<CameraShake>().ShakeCamera();
                gameObject.GetComponent<ErrorCounter>().AddError();
            }
            else if (hit.collider.tag == "CorrectSquare")
            {
                Debug.Log("Bien");
                gameObject.GetComponent<ErrorCounter>().ResetErrorCounter();
                _VfxsoundController.GetComponent<FeedBack>().PlayCorrectAnswerSound();
                _dataManager.GetComponent<DataManagement>().Acertar();
                _mosaico.GetComponent<MosaicoForm>().Dificulty();
                _mosaico.GetComponent<MosaicoForm>().GenerateAnswer();
            }
        }
    }
}
