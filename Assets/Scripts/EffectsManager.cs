using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectsManager : MonoBehaviour
{
    /*Para hacer un singleton haces una instancia est�tica. Ser� accesible desde todos lados, y ser� la "�nica" que se compartir�
    entre escenas */
    public static EffectsManager Intance;

    // Start is called before the first frame update
    void Awake()
    {
        /*Para vigilar que no se duplique, en el awake mira si ya hay alguna. 
         Si s� que hay se destruye autom�ticamente para que solo haya una*/
        if (EffectsManager.Intance == null)
        {
            //Si es null quiere decir que es la primera, por lo que se genera y se hace DontDestroyOnLoad
            EffectsManager.Intance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            //En caso contrario, quiere decir que ya hay otra, por lo que autom�ticamente se destruye
            Destroy(this.gameObject);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
