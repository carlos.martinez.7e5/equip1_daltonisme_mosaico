using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneManagement : MonoBehaviour
{
    private void Start()
    {
        _data = GameObject.Find("DataManager");
        //Debug.Log(Application.persistentDataPath);

        GameObject recordsBtn = GameObject.Find("Records_btn");

        Scene currentScene = SceneManager.GetActiveScene();
        if(currentScene.name == "TitleScene")
        {
            if (!File.Exists(Application.persistentDataPath + "/TopScores.save"))
            {
                recordsBtn.GetComponent<Button>().interactable = false;
                recordsBtn.GetComponent<Image>().color = Color.red;
            }
            else
            {
                recordsBtn.GetComponent<Button>().interactable = true;
                recordsBtn.GetComponent<Image>().color = Color.blue;
            }
        }
    }

    private GameObject _data; // El Data Manager. Se coje para saber en que estado se encuentra, actuar en consecuencia, y cambiarlo.

    public void Siguiente()
    {
        _data = GameObject.Find("DataManager");
        var color = _data.GetComponent<DataManagement>().CurrentColor;
        switch (color)
        {
            case DataManagement.Colores.azul:
                _data.GetComponent<DataManagement>().CurrentColor = DataManagement.Colores.rojo;
                SceneManager.LoadScene("RecordsScene");
                break;

            case DataManagement.Colores.rojo:
                _data.GetComponent<DataManagement>().CurrentColor = DataManagement.Colores.verde;
                SceneManager.LoadScene("MosaicoScene");
                break;

            case DataManagement.Colores.verde:
                _data.GetComponent<DataManagement>().CurrentColor = DataManagement.Colores.azul;
                SceneManager.LoadScene("MosaicoScene");
                break;
        }
    }

    public void GoToMenu()
    {
        GameObject.Destroy(_data);
        SceneManager.LoadScene("TitleScene");
    }

    public void StartTest()
    {
        SceneManager.LoadScene("MosaicoScene");
    }

    public void Records()
    {
       SceneManager.LoadScene("RecordsScene");
    }

    public void Help()
    {
        SceneManager.LoadScene("HelpScene");
    }

    public void Exit()
    {
        Application.Quit();
        //UnityEditor.EditorApplication.isPlaying = false;
    }
}
