using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class RecordsManager : MonoBehaviour
{
    [SerializeField] private Slider _sliderAzul;
    [SerializeField] private Text _porcentajeTxtAzul;

    [SerializeField] private Slider _sliderVerde;
    [SerializeField] private Text _porcentajeTxtVerde;

    [SerializeField] private Slider _sliderRojo;
    [SerializeField] private Text _porcentajeTxtRojo;
    
    private GameObject _data;

    void Start()
    {
        TopScores_Class tops = new TopScores_Class();
        if (File.Exists(Application.persistentDataPath + "/TopScores.save")) tops = SaveManager.LoadTopScores(); //Seg�n se inicie la escena, obtendr� los datos del r�cord
        else tops = new TopScores_Class();

        //Si enuentra un DataManager (quiere decir que ha jugado m�nimo uno)
        if (GameObject.Find("DataManager") != null) {
            
            //Primero se establecen los valores de la partida actual
            _data = GameObject.Find("DataManager");

            _sliderAzul.value = _data.GetComponent<DataManagement>().PorcentajeAzules;
            _porcentajeTxtAzul.text = _data.GetComponent<DataManagement>().PorcentajeAzules.ToString() + " %";

            _sliderVerde.value = _data.GetComponent<DataManagement>().PorcentajeVerdes;
            _porcentajeTxtVerde.text = _data.GetComponent<DataManagement>().PorcentajeVerdes.ToString() + " %";

            _sliderRojo.value = _data.GetComponent<DataManagement>().PorcentajeRojos;
            _porcentajeTxtRojo.text = _data.GetComponent<DataManagement>().PorcentajeRojos.ToString() + " %";

            GameObject dataManager = GameObject.Find("DataManager"); //Obtiene el componente
            var data = dataManager.GetComponent<DataManagement>();

            //Si alguno de los valores es m�s grande del que tiene guardados en top lo sobreescribe
            if (data.PorcentajeAzules > tops._azules) tops._azules = data.PorcentajeAzules;
            if (data.PorcentajeVerdes > tops._verdes) tops._verdes = data.PorcentajeVerdes;
            if (data.PorcentajeRojos > tops._rojos) tops._rojos = data.PorcentajeRojos;

            SaveManager.SaveTopScores(tops);
            Debug.Log("He guardado los datos");
        }
        else
        {
            _sliderAzul.value = tops._azules;
            _porcentajeTxtAzul.text = tops._azules.ToString() + " %";

            _sliderVerde.value = tops._verdes;
            _porcentajeTxtVerde.text = tops._verdes.ToString() + " %";

            _sliderRojo.value = tops._rojos;
            _porcentajeTxtRojo.text = tops._rojos.ToString() + " %";
        }
    }
}
